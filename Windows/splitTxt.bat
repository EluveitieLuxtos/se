@echo off
setlocal EnableDelayedExpansion
set "fileNumber=1"

:settings
set /p file=Quel fichier decouper ? 
set /p nblines=Combien de lignes par fichier ? 
set /p choix=Satisfait ? [y/n] 
if %choix% NEQ y (
	call :settings
)
if %nblines% EQU 0 (
	call :settings
)

:for_loop
for /f "tokens=1 delims=" %%i in ('type %file%') do set VrChain=%%i& call :write %%i
pause
goto end

:write
SET lineNumber=%~1
::echo %lineNumber%
::if "%~1"=="5" echo coucou
::if "%lineNumber%" EQU "5" (set /a "fileNumber+=1")
set /a "linetruncate=%lineNumber%/%nblines%*%nblines%"
if "%lineNumber%" EQU "%linetruncate%" (set /a "fileNumber+=1")
echo %VrChain%>> %file:~0,-4%%fileNumber%%file:~-4%

:end