@echo off

set deb=0
set choix=0
call :findfirstfile
:firstfilefound
call :setdeb
:debset
set /p saison=Numero de saison ?
for %%i in (*) do (
	if not "%%i" EQU "%~n0%~x0" call :rename "%%i"
)
pause
exit

:findfirstfile
for %%i in (*) do (
	set _file=%%i
	if not "%%i" EQU "%~n0%~x0" goto firstfilefound
)
goto firstfilefound

:setdeb
set /p deb=Combien de caracteres avant le numero xx d'episode ?
CALL SET substring=%%_file:~%deb%,2%%
ECHO %substring%
set /p choix=Satisfait ? [y/n]
if %choix% NEQ y (
	call :setdeb
)
goto debset

:rename
SET _donor=%~1
CALL SET substring=%%_donor:~%deb%,2%%
REN "%~1" S%saison%E%substring%%_donor:~-4%



